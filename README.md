---
license: unknown
configs:
- config_name: default
  data_files:
  - split: queries
    path: data/queries-*
  - split: corpus
    path: data/corpus-*
dataset_info:
  features:
  - name: _id
    dtype: string
  - name: text
    dtype: string
  - name: title
    dtype: string
  splits:
  - name: queries
    num_bytes: 2816
    num_examples: 54
  - name: corpus
    num_bytes: 4099348
    num_examples: 10446
  download_size: 2356188
  dataset_size: 4102164
---
